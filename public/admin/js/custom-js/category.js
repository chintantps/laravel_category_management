$( document ).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.myTable').DataTable();

    $("input").on("keypress", function(e) {    
        if (e.which === 32 && !this.value.length)        
            e.preventDefault();
    });
    /*$('#category_name').on('blur',function(){
        var category_name = $(this).val();
        var category_id = $("#category_id").val();
            
        $.ajax({
            url: "unique-category",
            type: "POST",
            data: {"category_name":category_name , "category_id":category_id},
            success: function(data){
                if( data.length > 0){
                    $('#error_message').text("Category already exist");
                    $('#save_category_btn').prop('disabled',true);
                    return false;
                }
                else{
                $('#error_message').text(" ");
                    $('#save_category_btn').prop('disabled',false);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                
            }
        });
    });*/


    $('#save_category_btn').on('click',function(){
        var category_id = $("#category_id").val();
        var category_name = $("#category_name").val();
        var should_form_submit = true;

        if( category_name == '' ){
            $("#category_name").css('border','1px solid red');
            should_form_submit = false;
        }

        $.ajax({
            url: "unique-category",
            type: "POST",
            data: {"category_name":category_name , "category_id":category_id},
            success: function(data){
                if( data.length > 0){
                    $('#error_message').text("Category already exist");
                    should_form_submit = false;
                }
                if( should_form_submit ){
                    $("#add_category_frm").submit();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                
            }
        });
    });

    // click of Add New Category button
    $('.add_category_btn').on('click',function(){
        
        //$("#category_name").val(" ").css('border','');
        $("#category_name").val('').css('border','');
    });

    // click of edit edit category icon
    $('.edit_category_btn').on('click',function(){
        var category_id = $(this).data('id');
        var category_name = $(this).data('name');

        $("#add_category_frm #category_id").val(category_id);
        $("#add_category_frm #category_name").val(category_name);
    });

    // click of remove category icon
    $('.remove_category_btn').on('click',function(){
        var category_id = $(this).data('id');
        var item_counts = $(this).data('item-counts');
        var post_url = $(this).data('url');
        if( item_counts > 0 ){
            swal({
                title: 'Item Exist',
                text: "You cannot remove this category",
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'OK'
            });
        }else{
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'OK'
            }).then(function( isConfirm) {
                if(isConfirm){
                    $.ajax({
                        url: post_url,
                        type: "POST",
                        data: {
                            "id": category_id,
                            "_method": 'DELETE',
                        },
                        success: function (data) {
                            swal("Done!", "It was succesfully deleted!", "success");
                            $('.swal2-confirm').on('click',function(){
                                window.location.reload();
                            });
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            swal("Error deleting!", "Please try again", "error");
                        }
                    });
                }
            });
        }
    });       
});    
