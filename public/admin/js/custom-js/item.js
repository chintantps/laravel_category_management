
$( document ).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.myTable').DataTable();
    $("input").on("keypress", function(e) {    
        if (e.which === 32 && !this.value.length)        
            e.preventDefault();
    });

    $('#save_item_btn').on('click',function(){
        
        var item_id = $("#item_id").val();
        var item_name = $("#item_name").val();
        var item_desc = $("#item_desc").val();    
        var category_ids = $("#category_ids").val();

        if( item_name == '' || item_name == null || category_ids == '' || category_ids == null ){

            if( item_name == '' || item_name == null ){
                $("#item_name").css('border','1px solid red');    
            }
            if( category_ids == '' || category_ids == null ){
                $("#category_ids").css('border','1px solid red');    
            }
        }else{
            $("#add_item_frm").submit();
        }
    });

    // click of Add New item button
    $('.add_item_btn,.edit_item_btn').on('click',function(){
        $("#item_id").val('');
        $("#item_name").val('').css('border','');
        $("#item_desc").val('');
        $("#category_ids").val('').css('border','');
    });

    // click of edit edit category icon
    $('.edit_item_btn').on('click',function(e){
        e.preventDefault();
        var item_id = $(this).data('id');
        var item_name = $(this).data('name');
        var item_desc = $(this).data('description');
        var item_categories = ($(this).data('categories') + '').split(',');
           
        $("#add_item_frm #item_id").val(item_id);
        $("#add_item_frm #item_name").val(item_name);
        $("#add_item_frm #item_desc").val(item_desc);
        $('#add_item_frm #category_ids').val(item_categories);
    });

    // click of remove category icon
    $('.remove_item_btn').on('click',function(){
        var item_id = $(this).data('item-id');
        var post_url = $(this).data('url');
            
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK'
        }).then(function(isConfirm) {
            if(isConfirm){
                $.ajax({
                    url: post_url,
                    type: "POST",
                    data: {
                        "id": item_id,
                        "_method": 'DELETE',
                    },
                    success: function (data) {
                        swal("Done!", "It was succesfully deleted!", "success");
                        $('.swal2-confirm').on('click',function(){
                            window.location.reload();
                        });
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error deleting!", "Please try again", "error");
                    }
                });
            }
        });
    }); 
});    
