<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'PageController@index')->name('front.home');
Route::get ( '/redirect/{service}', 'SocialAuthController@redirect' );
Route::get ( '/callback/{service}', 'SocialAuthController@callback' );

Route::group([ 'middleware'=>['auth'] ],function(){
    Route::resource('category', 'CategoryController');
    Route::post('unique-category', 'CategoryController@validateDuplicateName')->name('unique-category');
    Route::resource('items', 'ItemController');
    Route::post('unique-item', 'CategoryController@validateDuplicateName')->name('unique-item');
    Route::get('/home', 'HomeController@index')->name('home');
});


