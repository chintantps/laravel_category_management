<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Laravel Demo</title>
    <link href="{{ asset('admin/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/css/login.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/css/reset_password.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/css/color_theme.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/css/custom_dev.css') }}" rel="stylesheet">

</head>

<body class="login_body" background="{{ asset('admin/images/login_bg_option1.png') }}">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="logo-wrapper">
                        <a href="#" class="project-title">
                            Laravel Demo
                        </a>
                    </div>
                </div>
            </div>

            @yield('content')
        
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('admin/js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/jquery.validate.js') }}"></script>
    <script src="{{ asset('admin/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('admin/js/retina.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
      $(function(){
        $("#login").validate({
          rules:{
            inputemail:{required:true, email:true},
            pwd:{required:true},
          },
          messages: {
                "inputemail": {
                    required: "Please Enter Email-ID"
                },
                "pwd": {
                    required: "Please Enter Password"
                }
            }
        });
      });
    </script>
</body>

</html>
