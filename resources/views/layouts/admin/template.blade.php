<script type="text/javascript"> var base_url = '';</script>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="minimal-ui, width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Laravel Demo</title>
    <link href="{{ asset('admin/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/font-awesome.css') }}">
    <link href="{{ asset('admin/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/css/default.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/css/demo-form.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/css/add_new_school.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/css/view_reports.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/css/sweetalert2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/css/color_theme.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/css/custom_dev.css') }}" rel="stylesheet">
    <link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">

    <script type="text/javascript" src="{{ asset('admin/js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/sweetalert2.min.js') }}"></script>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
</head>

<body>
    <div class="page-wrapper">
        <div id="wrapper" class="toggled">
        <!-- Sidebar Start-->
        <ul class="sidebar_wrap navbar navbar-inverse navbar-fixed-top col-xs-12" id="sidebar-wrapper" role="navigation">
            <li class="col-xs-12">
                <a href="{{ route('category.index') }}" class="main-link col-xs-12" title="Users">                        
                    <span><i class="fa fa-users"></i><span>Category</span></span>
                </a>
            </li>

            <li class="col-xs-12">
                <a href="{{ route('items.index') }}" class="main-link col-xs-12" title="Students">                        
                    <span><i class="fa fa-graduation-cap"></i><span>Items</span></span>
                </a>
            </li>
        </ul>
        <!-- Sidebar End-->
        <!-- Main page content including header-->
        <div id="page-content-wrapper">
            <!-- Header start -->
            <div class="header_wrap navbar-fixed-top">
                <div class="menu_icon" class="hamburger is-closed" id="leftbtn">
                    <i class="fa fa-bars"></i>
                </div>
                <div class="logo">
                    <a href="#"></a>
                </div>
                <div class="my_account">
                    <div class="btn-group account_wrap">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <span class=""><i class="fa fa-user"></i></span> <span class="u_name hidden-xs"> {{ Auth::user()->name }} </span><span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu account_dropdown" role="menu">
                            <li>
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Header End-->

            @yield('content') 

            <!-- Footer starts here-->
            <div class="col-xs-12 footer_wrap">
            All Rights Reserved | ©Example App
            </div>
        </div>    
        </div>
    </div>
    @yield('js')  
</body>
<script src="{{ asset('admin/js/bootstrap.min.js') }}" type="text/javascript"></script>
</html>
<!-- footer endd here -->  