@extends('layouts.admin.template_auth')
@section('content')
<div class="reset-form-wrap">
    <div class="reset_box_wrap">
        <h1>
            {{ __('Reset Password') }}
        </h1>
        <form method="POST" action="{{ route('password.update') }}" class="form" autocomplete="on" id="resetpassword" >
            @csrf
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <div class="form-group login_input forgot_input">
                <input type="hidden" name="token" value="{{ $token }}">
                <input id="email" type="email" placeholder="Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group login_input forgot_input">
                <input id="password" type="password" placeholder="Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group login_input forgot_input">
                <input id="password-confirm" type="password" placeholder="Confirm Password" class="form-control" name="password_confirmation" required>
            </div>
            <div class="login_btn">
                <button type="submit" class="btn btn-default">Reset Password</button>
            </div>
            
            <div class="reset_links">
                <a href="login.html">Back to Login Page</a>
            </div>
        </form>
    </div>
</div>
@endsection
