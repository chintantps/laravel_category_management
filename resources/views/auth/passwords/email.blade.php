@extends('layouts.admin.template_auth')
@section('content')


<div class="reset-form-wrap">
    <div class="reset_box_wrap">
        <h1>
            Forgot Password
        </h1>
        <form method="POST" action="{{ route('password.email') }}" class="form" autocomplete="on" id="resetpassword" >
            @csrf
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <div class="form-group login_input forgot_input">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="login_btn">
                <button type="submit" class="btn btn-default">Reset Password</button>
            </div>
            
            <div class="reset_links">
                <a href="login.html">Back to Login Page</a>
            </div>
        </form>
    </div>
</div>

@endsection
