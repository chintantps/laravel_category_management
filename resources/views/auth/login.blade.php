@extends('layouts.admin.template_auth')
@section('content')

<div class="row">
    <div class="col-md-10 col-md-offset-1 col-sm-12">
        <form method="POST" action="{{ route('login') }}" class="form-inline">
            @csrf
            <div class="login_box_wrap login_box_wrap_v1">
                @if( Session::has( 'notification_msg' ))
                    <div class="alert alert-success">
                        {{ Session::get( 'notification_msg' ) }}
                    </div>                    
                @endif
                <div class="form-group col-md-5 col-sm-5 col-xs-12  login_input">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                
                <div class="form-group col-md-5 col-sm-5 col-xs-12 login_input pw_input">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-2 col-sm-2 col-sm-12 login_btn">
                    <button type="submit" class="btn btn-default">
                        {{ __('Login') }}
                    </button>
                </div>
                <!--<div class="row">
                    <div class="col-xs-12  pl-none">
                    <p class="errmsg">The email and password you entered don't match.</p>
                    </div>
                </div> -->
                <div class="row">
                    <div class="col-xs-12 login_links forgot_link pl-none">
                        <a href="{{ route('password.request') }}">
                            Forgot password ?
                        </a>
                    </div>
                </div>
                <div id="social_login_box">
                    <div class="row facebook-login social-login-link">
                        <a href="redirect/facebook" class="loginBtn loginBtn--facebook">
                            Login in with Facebook
                        </a>
                    </div>
                    <div class="row google-login social-login-link">
                        <a href="redirect/google" class="loginBtn loginBtn--google">
                            Login in with Google
                        </a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection
        