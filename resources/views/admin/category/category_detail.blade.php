@extends('layouts.admin.template')
@section('content')

<!-- Header End-->
<div class="row">
    <div class=" col-xs-12 col-sm-12">
        <!-- <div class="inner_page_wrap"> -->
            <div class="page_title view-page-title">
                <h2 class="mt-10">Category Details</h2>
            </div>
        <!-- </div> -->
    </div>
</div>
<!-- page title row ends here-->

<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="panel panel-default">
            <ul class="list-group">
                <li class="list-group-item">
                    <div class="row custom-form-row-full">
                        <a href="javascript:;" class="edit_category_btn action-link btn custom-btn custom-add-bordered-btn m-none" data-toggle="modal" data-target="#category_modal" data-id="{{ $oCategoryDetail->id }}" data-name="{{ $oCategoryDetail->category_name }}">
                            Edit
                        </a>
                        <a href="javascript:;" class="remove_category_btn action-link btn custom-btn custom-add-bordered-btn m-none" data-id="{{ $oCategoryDetail->id }}" data-url="{{ route('category.destroy',$oCategoryDetail->id) }}" data-item-counts="{{ count($oCategoryDetail->items) }}">
                            Delete
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<!-- table wrapper starts here -->
<div class="row table-contents-wrapper table-records-wrapper">
    <div class="col-xs-12 revenue-table-wrapper">
        <div class="table-responsive custom-record-table">
            <table class="table table-hover revenue-table">
                <thead class="bg-color">
                    
                </thead>
                <tbody>
                    <tr>
                        <th>Category Name</th>
                        <td>{{ $oCategoryDetail->category_name }}</td>
                    </tr>
                    <tr>
                        <th>Created At</th>
                        <td>{{ date('d M , Y',strtotime($oCategoryDetail->created_at)) }}</td>
                    </tr>
                    <tr>
                        <th>Updated at</th>
                        <td>{{ date('d M , Y',strtotime($oCategoryDetail->updated_at)) }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- table wrapper ends here -->

@include('admin.modals.category_modal')
<!-- table wrapper ends here -->
@endsection
@section('js')
  <script src="{{ asset('admin/js/custom-js/category.js') }}"></script>
@endsection