@extends('layouts.admin.template')
@section('content')

<!-- Header End-->
<div class="row">
    <div class=" col-xs-12 col-sm-12">
        <!-- <div class="inner_page_wrap"> -->
        <div class="page_title view-page-title">
            <h2 class="mt-10">Categories</h2>
        </div>
        <!-- </div> -->
    </div>
</div>
<!-- page title row ends here-->

<!-- form -->
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="panel panel-default">
            <ul class="list-group">
                <li class="list-group-item">
                    <div class="row custom-form-row-full">
                        <button data-target="#category_modal" class="add_category_btn action-link btn custom-btn custom-add-bordered-btn m-none" data-toggle="modal" href="javascript:;"><span class="glyphicon glyphicon-plus"></span> Add New Category</button>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- table wrapper starts here -->
<div class="row table-contents-wrapper table-records-wrapper">
    <div class="col-xs-12 revenue-table-wrapper">
        <div class="table-responsive custom-record-table">
            <table class="table table-hover revenue-table myTable">
                <thead class="bg-color">
                    <tr>
                        <th>Category name</th>
                        <th>No of Items</th>
                        <th class="width-10">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $oCategoriesList as $key => $oCategoryInfo )
                        <tr>
                            <td>{{ $oCategoryInfo->category_name }}</td>
                            <td>{{ count($oCategoryInfo->items) }}</td>
                            <td>
                                <a href="javascript:;" class="action-link edit_category_btn" data-toggle="modal" data-target="#category_modal" data-id="{{ $oCategoryInfo->id }}" data-name="{{ $oCategoryInfo->category_name }}">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                </a>
                                <a href="javascript:;" class="action-link remove_category_btn" data-id="{{ $oCategoryInfo->id }}" data-url="{{ route('category.destroy',$oCategoryInfo->id) }}" data-item-counts="{{ count($oCategoryInfo->items) }}">
                                    <i class="glyphicon glyphicon-trash"></i> 
                                </a>
                                <a href="{{ route('category.show',$oCategoryInfo->id) }}" class="action-link">
                                    <i class="glyphicon glyphicon-eye-open"></i>
                                </a>
                            </td>
                        </tr>                      
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@include('admin.modals.category_modal')
<!-- table wrapper ends here -->
@endsection
@section('js')
  <script src="{{ asset('admin/js/custom-js/category.js') }}"></script>
@endsection