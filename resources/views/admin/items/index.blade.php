@extends('layouts.admin.template')
@section('content')

<!-- Header End-->
<div class="row">
    <div class=" col-xs-12 col-sm-12">
        <div class="page_title view-page-title">
            <h2 class="mt-10">Items</h2>
        </div>
    </div>
</div>
<!-- page title row ends here-->

<!-- form -->
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="panel panel-default">
            <ul class="list-group">
                <li class="list-group-item">
                    <div class="row custom-form-row-full">
                        <button data-target="#item_modal" class="add_item_btn action-link btn custom-btn custom-add-bordered-btn m-none" data-toggle="modal" href="javascript:;"><span class="glyphicon glyphicon-plus"></span> Add New Item</button>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<!-- table wrapper starts here -->
<div class="row table-contents-wrapper table-records-wrapper">
    <div class="col-xs-12 revenue-table-wrapper">
        <div class="table-responsive custom-record-table">
            <table class="table table-hover revenue-table myTable">
                <thead class="bg-color">
                    <tr>
                        <th>Item name</th>
                        <th>Description</th>
                        <th class="width-10">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($oItemList as $key => $oItem)
                    <tr>
                        <td>{{ $oItem->item_name }}</td>
                        <td>{{ $oItem->item_description }}</td>
                        <td>
                            <a href="javascript:;" data-target="#item_modal" class="action-link edit_item_btn" data-id="{{ $oItem->id }}" data-name="{{ $oItem->item_name }}" data-description="{{ $oItem->item_description }}" data-categories="{{ implode(',',$oItem->categories->pluck('id')->toArray()) }}"  data-toggle="modal">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </a>
                            <a href="javascript:;" class="action-link remove_item_btn"  data-item-id="{{ $oItem->id }}" data-url="{{ route('items.destroy',$oItem->id) }}">
                                <i class="glyphicon glyphicon-trash"></i> 
                            </a>
                            <a href="{{ route('items.show',$oItem->id) }}" class="action-link">
                                <i class="glyphicon glyphicon-eye-open"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


@include('admin.modals.item_modal')
@endsection
@section('js')
  <script src="{{ asset('admin/js/custom-js/item.js') }}"></script>
@endsection

