@extends('layouts.admin.template')
@section('content')

<!-- Header End-->
<div class="row">
    <div class=" col-xs-12 col-sm-12">
        <!-- <div class="inner_page_wrap"> -->
            <div class="page_title view-page-title">
                <h2 class="mt-10">Item Details</h2>
            </div>
        <!-- </div> -->
    </div>
</div>
<!-- page title row ends here-->

<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="panel panel-default">
            <ul class="list-group">
                <li class="list-group-item">
                    <div class="row custom-form-row-full">
                        <a href="javascript:;" data-target="#item_modal" class="edit_item_btn action-link btn custom-btn custom-add-bordered-btn m-none" data-id="{{ $oItemDetail->id }}" data-name="{{ $oItemDetail->item_name }}" data-description="{{ $oItemDetail->item_description }}" data-categories="{{ implode(',',$oItemDetail->categories->pluck('id')->toArray()) }}"  data-toggle="modal">
                            Edit
                        </a>
                        <a href="javascript:;" class="remove_item_btn action-link btn custom-btn custom-add-bordered-btn m-none"  data-item-id="{{ $oItemDetail->id }}" data-url="{{ route('items.destroy',$oItemDetail->id) }}">
                            Delete
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<!-- table wrapper starts here -->
<div class="row table-contents-wrapper table-records-wrapper">
    <div class="col-xs-12 revenue-table-wrapper">
        <div class="table-responsive custom-record-table">
            <table class="table table-hover revenue-table">
                <thead class="bg-color">
                    
                </thead>
                <tbody>
                    <tr>
                        <th>Item Name</th>
                        <td>{{ $oItemDetail->item_name }}</td>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <td>{{ $oItemDetail->item_description }}</td>
                    </tr>
                    <tr>
                        <th>Categories</th>
                        <td>{{ implode( ' , ',$oItemDetail->categories->pluck('category_name')->toArray() ) }}</td>
                    </tr>
                    <tr>
                        <th>Created At</th>
                        <td>{{ date('d M , Y',strtotime($oItemDetail->created_at)) }}</td>
                    </tr>
                    <tr>
                        <th>Updated at</th>
                        <td>{{ date('d M , Y',strtotime($oItemDetail->updated_at)) }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- table wrapper ends here -->

@include('admin.modals.item_modal')
<!-- table wrapper ends here -->
@endsection
@section('js')
  <script src="{{ asset('admin/js/custom-js/item.js') }}"></script>
@endsection