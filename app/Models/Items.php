<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Items extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'item_name','item_description'
    ];

    public function categories(){
        return $this->belongsToMany( Categories::class , 'items_categories' , 'item_id' , 'category_id' );
    }
}
