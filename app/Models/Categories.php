<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categories extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'category_name'
    ];

    public function items(){
        return $this->belongsToMany( Items::class , 'items_categories' , 'category_id' , 'item_id');
    }
}
