<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialite;
use Redirect;
use Auth;
use App\Models\User;

class SocialAuthController extends Controller
{
    public function redirect($service) {
        return Socialite::driver ( $service )->redirect ();
    }
    public function callback($service) {
        $oUser = Socialite::with ( $service )->user();
        $objUser = User::where('email',$oUser->email)->first();
        
        $aInputs['provider'] = $service;
        $aInputs['provider_id'] = $oUser->id;

        if(!isset($objUser)){
            $aInputs['name'] = $oUser->name;
            $aInputs['email'] = $oUser->email;
            $aInputs['profile_image'] = $oUser->avatar;
            $aInputs['password'] = bcrypt('123456');
            $aInputs['remember_token'] = str_random(30);
                
            // Insert - users table
            User::create($aInputs);
			
            /*if( $objUser )
                Mail::to($objUser->email)->send(new SignupMail($objUser));*/
		}else{
			// Update - users table
			$objUser->update($aInputs);
		}
		
		if(isset($objUser)){
			Auth::login($objUser);
			return redirect()->route('category.index');
		}
    }
}
