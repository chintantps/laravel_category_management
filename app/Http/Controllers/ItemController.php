<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Items;
use App\Models\Categories;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $oItemList = Items::all();
        $oCategories = Categories::all();
        return view('admin.items.index',compact('oItemList','oCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!empty($request->id)){
            $objItem = Items::find($request->id);
            $objItem->update($request->all());
            $objItem->categories()->sync( $request->category_ids );
        }else{
            Items::create( $request->all() )->categories()->sync( $request->category_ids );
        }
        return redirect()->route('items.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $oItemDetail = Items::with(['categories'])->find($id);
        $oCategories = Categories::all();
        return view('admin.items.item_detail',compact('oItemDetail','oCategories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Items $item)
    {
        if(!empty($item->id)){
            $oItem = Items::with(['categories'])->find($item->id);
            $oItem->categories()->detach( $oItem->categories()->pluck('item_id') );
            $oItem->delete();
        }
    }
}
