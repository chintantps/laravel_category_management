<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categories;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $oCategoriesList = Categories::with(['items'])->get();
        return view('admin.category.index',compact('oCategoriesList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!empty($request->id)){
            Categories::find($request->id)->update($request->all());
        }else{
            Categories::create( $request->all() );
        }
        return redirect()->route('category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $oCategoryDetail = Categories::find($id);
        return view('admin.category.category_detail',compact('oCategoryDetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categories $category)
    {
        Categories::find($category->id)->delete();
    }
    public function validateDuplicateName(Request $request){
        if(!empty($request->category_id)){
            $oCategory = Categories::where('category_name',$request->category_name)->where('id','<>',$request->category_id)->get();
        }else{
            $oCategory = Categories::where('category_name',$request->category_name)->get();
        }
        return response()->json($oCategory);
    }
}
